from setuptools import setup

setup(
	name='mudiscord',
	version='0.0.1',
	description='A supporting bot for xmenrevolution.com',
	entry_points = {
		'console_scripts': ['mudiscord=mudiscord.mudiscord:run']
	},
	zip_safe=False,
	packages=['mudiscord'],
	install_requires=[
		'discord.py',
	]
)
