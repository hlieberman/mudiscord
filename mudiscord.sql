CREATE TABLE logs (rowid INTEGER PRIMARY KEY AUTOINCREMENT, pose TEXT, channel INTEGER, m_id INTEGER);
CREATE INDEX logchan ON logs (channel);
CREATE INDEX logmid ON logs (m_id);
CREATE TABLE areas (name TEXT COLLATE NOCASE, short TEXT COLLATE NOCASE, loc_id INTEGER, description BLOB, unlisted BOOLEAN);
CREATE UNIQUE INDEX nameshort ON areas (name, short);
CREATE INDEX locations ON areas (loc_id, unlisted);
