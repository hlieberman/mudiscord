import asyncio
import datetime
import discord  # type: ignore
import logging
import random
import re
import os
import sqlite3
import sys
import tempfile
import traceback

class MUDiscord:
    def __init__(self):
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
        logging.info("Initializing...")
        try:
            self.token = os.environ["DISCORD_TOKEN"]
        except KeyError:
            logging.error("No Discord token found!")
            sys.exit(1)

        self.db = sqlite3.connect(
            os.getenv("SQLITE_DB_PATH", "/var/lib/mudiscord/mudiscord.sqlite3")
        )

        intents = discord.Intents.default()
        intents.members = True
        intents.guild_messages = True
        intents.message_content = True
        self.client = discord.Client(intents=intents)
        self.setup_funcs()

        # Restore scene info from table
        c = self.db.cursor()
        self.monitored_channels = dict()
        for chan in c.execute("SELECT DISTINCT channel, m_id FROM logs"):
            self.monitored_channels[chan[0]] = chan[1]

    def run(self):
        logging.info("Connecting...")
        self.client.run(self.token)

    def setup_funcs(self):
        @self.client.event
        async def on_error(event, args):
            logging.warn(
                "Error! Event %s failed with args %s! Python says: %s",
                event,
                args,
                traceback.format_exc(),
            )

        @self.client.event
        async def on_ready():
            self.guild = self.client.guilds[0]
            logging.info(
                "Login successful as %s (%s)",
                self.client.user.name,
                self.client.user.id,
            )

        @self.client.event
        async def on_message(message):
            logging.debug("Got message %s", message.id)
            # First, filter out messages that the bot sent
            # to keep from going into an infinite loop
            if message.author.id == self.client.user.id:
                logging.debug("Talking to myself.")
                return None
            elif message.content.startswith("?desc"):
                logging.debug("Desc command found.")
                await process_desc(message)
            elif message.content.startswith("?clear"):
                logging.debug("Clear command found.")
                await clear_page(message)
            elif message.content.startswith("?log"):
                logging.debug("Logging command found.")
                await process_log(message)
            elif message.content.startswith("?role"):
                logging.debug("Found role set command.")
                await process_role(message)
            elif message.content.startswith("?help"):
                logging.debug("Found help command.")
                await process_help(message)
            elif message.content.startswith("?finger"):
                logging.debug("Found finger command.")
                await process_finger(message)
            elif message.content.startswith("?alt"):
                logging.debug("Found alt command.")
                await process_alt(message)
            elif message.content.startswith("?flip"):
                logging.debug("Found coinflip.")
                await process_coinflip(message)
            elif message.content.startswith("?roll"):
                logging.debug("Found diceroll.")
                await process_diceroll(message)
            else:
                logging.debug("Not to me.")

        async def clear_page(message):
            logging.debug("Told to clear %s", message.content[7:])
            if not check_role(message.author.id, "Wizards"):
                await send_forbidden(message)
                return None

            if message.clean_content[8:] == message.channel.name:
                await message.channel.purge(
                    check=lambda x: not x.pinned, bulk=True, limit=1000
                )
            else:
                await send_failure(message)

        def check_finger_write(message, character):
            # In order to write to the finger, one of the following must be true:
            # 1. You're a Wizard (in which case you have no restrictions), or
            # 2. You're a Member (in which case, you can write to any of your alts, or anyone who isn't anyone's alt), or
            # 3. You're something else, in which case, you can write to only your alts

            c = self.db.cursor()
            member = message.author

            if check_role(member.id, "Wizards"):
                return True

            c.execute("SELECT player FROM alts where alt = ?", [character])
            alts = c.fetchall()
            if (str(hash(member)),) in alts:
                return True
            elif check_role(member.id, "Member") and not alts:
                return True
            else:
                return False

        def check_role(user_id: int, role: str) -> bool:
            member = self.guild.get_member(user_id)
            if discord.utils.find(lambda x: str(x) == role, member.roles):
                return True
            else:
                return False

        async def desc_modify(message):
            c = self.db.cursor()
            comm = re.match(
                "^\?desc (?P<command>add|edit|delete) (?P<args>[^$]+)", message.content
            ).groupdict()
            if comm["command"] == "add":
                args = re.match(
                    "\<(?P<borough>[A-Z]{3})\> (?P<name>[^\(]+) \((?P<unlisted>\*?)(?P<short>[A-Z]{3})\) - (?P<neighborhood>[^=]+) = (?P<desc>[^$]+)",
                    comm["args"],
                ).groupdict()
                c.execute(
                    "SELECT short FROM areas WHERE short = ? OR name = ? or borough = ?",
                    [args["short"], args["name"], args["short"]],
                )
                if c.fetchone():
                    logging.info("Refusing to insert duplicate description.")
                    await send_failure(message)
                else:
                    logging.info(
                        "Inserting new description for area %s (%s) in %s.",
                        args["name"],
                        args["short"],
                        args["neighborhood"],
                    )
                    c.execute(
                        "INSERT INTO areas (name, short, description, borough, neighborhood, unlisted) VALUES (?, ?, ?, ?, ?, ?)",
                        [
                            args["name"],
                            args["short"],
                            args["desc"],
                            args["borough"],
                            args["neighborhood"].lower(),
                            bool(args["unlisted"]),
                        ],
                    )
                    self.db.commit()
                    await send_success(message)
            elif comm["command"] == "edit":
                args = re.match(
                    "(?P<short>[A-Z]{3}) ?= ?(?P<desc>[^$]+)", comm["args"]
                ).groupdict()
                c.execute("SELECT rowid FROM areas WHERE short = ?", [args["short"]])
                row = c.fetchone()
                if row:
                    logging.info("Editing description of area %s", args["short"])
                    c.execute(
                        "UPDATE areas SET description = ? WHERE rowid = ?",
                        [args["desc"], row[0]],
                    )
                    self.db.commit()
                    await send_success(message)
                else:
                    await send_failure(message)
            elif comm["command"] == "delete":
                args = comm["args"]
                c.execute(
                    "SELECT rowid FROM areas WHERE name = ? OR short = ?", [args, args]
                )
                row = c.fetchone()
                if row:
                    logging.info("Deleting description for area %s", args)
                    c.execute("DELETE FROM areas WHERE rowid = ?", [row[0]])
                    self.db.commit()
                    await send_success(message)
                else:
                    await send_failure(message)
            else:
                await send_failure(message)

        async def process_alt(message):
            c = self.db.cursor()

            if message.content.startswith("?alt list"):
                c.execute(
                    "SELECT alt FROM alts WHERE player = ?", [hash(message.mentions[0])]
                )
                msgs = [x[0].title() for x in c.fetchall()]
                msgs.sort()
                await message.channel.send(
                    "The player has the following alts: {}".format(", ".join(msgs))
                )
            else:
                if check_role(message.author.id, "Wizards"):
                    match = re.match(
                        "\?alt (add|remove) (?P<character>[\w\-\.]+)", message.content
                    )
                    if not match:
                        await send_failure(message)
                        return

                    ar = match.group(1)
                    char = match.group(2).lower()

                    if ar == "add":
                        if not message.mentions:
                            await send_failure(message)
                            return
                        c.execute("SELECT alt FROM alts WHERE alt = ?", [char])
                        if c.fetchone():
                            # Duplicate alt
                            await send_failure(message)
                            await message.add_reaction(
                                "\N{CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS}"
                            )
                            return
                        c.execute(
                            "INSERT INTO alts (alt, player) VALUES (?, ?)",
                            [char, hash(message.mentions[0])],
                        )
                    elif ar == "remove":
                        c.execute("DELETE FROM alts WHERE alt = ?", [char])
                    self.db.commit()
                    await send_success(message)
                else:
                    await send_forbidden(message)

        async def process_coinflip(message):
            choices = ["Heads", "Tails"]
            await message.channel.send(
                "Coin flip is: {}".format(random.choice(choices))
            )

        async def process_desc(message):
            c = self.db.cursor()

            # If it's just the ?desc...
            if message.content == "?desc":
                logging.debug("Getting all the descs.")
                c.execute(
                    "SELECT name, short, neighborhood FROM areas WHERE unlisted IS NOT 1 ORDER BY neighborhood DESC, name DESC"
                )
                areas = c.fetchall()
                msgs = ["{} ({}) - {}".format(x[0], x[1], x[2].title()) for x in areas]
                await send_long_message(msgs, message)
            elif re.match("^\?desc (add|edit|delete)", message.content):
                # Wizards are the only ones who can edit descs
                if check_role(message.author.id, "Wizards"):
                    await desc_modify(message)
                else:
                    await send_forbidden(message)
            else:
                # If we're not adding, editing, or
                # deleting, we must be trying to
                # retrieve a desc
                term = message.content[6:]
                c.execute(
                    "SELECT name, borough, neighborhood, description FROM areas WHERE name = ? OR short = ?",
                    [term, term],
                )
                res = c.fetchone()
                if res:
                    # Got a direct match for a desc
                    msg = "**<{}> {} - {}**\n{}".format(
                        res[1], res[0], res[2].title(), res[3]
                    )
                    await message.channel.send(msg)
                    return

                # Try for a neighborhood
                c.execute(
                    "SELECT name, short FROM areas WHERE neighborhood = ? AND unlisted IS NOT 1 ORDER BY name DESC",
                    [term],
                )
                res = c.fetchall()
                if res:
                    await message.channel.send(
                        "**Rooms in {}:**\n".format(term.title())
                    )
                    msgs = ["{} **({})**".format(x[0], x[1]) for x in res]
                    await send_long_message(msgs, message)
                    return

                # Try for a borough/type
                c.execute(
                    "SELECT name, short, neighborhood FROM areas WHERE borough = ? AND unlisted IS NOT 1 ORDER BY neighborhood DESC,name DESC",
                    [term],
                )
                res = c.fetchall()
                if res:
                    await message.channel.send(
                        "**Rooms in {}:**\n".format(term.upper())
                    )
                    msgs = [
                        "{} - {} **({})**".format(x[0], x[2].title(), x[1]) for x in res
                    ]
                    await send_long_message(msgs, message)
                    return

                # We have no matches.
                await message.channel.send(
                    "I couldn't find a desc or neighborhood for {}".format(term)
                )

        async def process_diceroll(message):
            dice = re.search(" (?P<num>\d+)d(?P<die>\d+)", message.content)
            if not dice or dice.lastindex != 2:
                await send_failure(message)
                return

            num = int(dice["num"])
            die = int(dice["die"])
            if num >= 50 or die >= 1000:
                await send_failure(message)
                return

            rolls = []
            for _ in range(num):
                rolls.append(random.randrange(1, die + 1))

            await message.channel.send(
                f"Rolled {num} {die}-sided dice and got {rolls} for a total of {sum(rolls)}."
            )

        async def process_finger(message):
            c = self.db.cursor()

            if message.content.startswith("?finger set"):
                match = re.match(
                    "\?finger set (?P<char>[\w\-\.]+) (?P<attrib>[\w ]+) ?= ?(?P<value>[^$]+)",
                    message.content,
                )
                if not match:
                    await send_failure(message)
                    return

                char = match.group("char").lower()
                attrib = match.group("attrib").strip().lower()
                value = match.group("value").strip()

                if not check_finger_write(message, char):
                    await send_forbidden(message)
                else:
                    c.execute(
                        "INSERT INTO finger (character, attribute, value) VALUES (?, ?, ?)"
                        "ON CONFLICT(character, attribute) DO UPDATE SET value = excluded.value",
                        [char, attrib, value],
                    )
                    self.db.commit()
                    await send_success(message)
            elif message.content.startswith("?finger unset"):
                match = re.match(
                    "\?finger unset (?P<char>[\w\-\.]+) (?P<attrib>[\w ]+)",
                    message.content,
                )
                if not match:
                    await send_failure(message)
                    return

                char = match.group("char").lower()
                attrib = match.group("attrib").strip().lower()

                if not check_finger_write(message, char):
                    await send_forbidden(message)
                else:
                    c.execute(
                        "DELETE FROM finger WHERE character = ? AND attribute = ?",
                        [char, attrib],
                    )
                    self.db.commit()
                    await send_success(message)
            else:
                match = re.match("\?finger (?P<char>[\w\-\.]+)$", message.content)
                if not match:
                    await send_failure(message)
                    return
                char = match.group("char").lower()
                c.execute(
                    "SELECT attribute, value FROM finger WHERE character = ? ORDER BY attribute DESC",
                    [char],
                )
                fingers = c.fetchall()
                if not fingers:
                    await send_success(message)
                else:
                    await message.channel.send("Finger for {} is:\n".format(char))
                    msgs = ["**{}**: {}".format(x[0].title(), x[1]) for x in fingers]
                    await send_long_message(msgs, message)

        async def process_help(message):
            # Giant elif block, because I've given up on life apparently
            if message.content == "?help":
                await message.channel.send(
                    "I know several commands!\n\n"
                    "`?alt` is a wizard-only command to set a user's alts.	`?alt add Alt @user` to add, and `?alt remove Alt @user` to remove.\n"
                    "`?clear #channelname` will remove all messages in a channel.  For safety, you must use it in the channel that you're trying to clear.	Be careful -- there's no going back!\n"
                    '`?desc` is for setting and getting "room" descriptions.  Use `?help desc` for more information.\n'
                    "`?finger` is for setting and getting finger attributes on a character.	 Use `?help finger` for more information.\n"
                    "`?log` is for turning on the auto-logger.  Use `?log start` to start it, and `?log end` when the scene is over.  You may also end the log with ?log oocend if you want to preserve the log in full without changes.\n"
                    "`?role` will let you grant yourself a role, once you've been here long enough to be a full member.  `?role add` to add, and `?role remove` to remove.\n"
                    "`?finger` and `?role` can be done in private message.	All other commands must be done in a server channel.\n"
                    "`?flip` and `?roll 1d6` will flip a coin and/or roll arbitrary dice for you."
                )
            elif message.content == "?help desc":
                await message.channel.send(
                    "For most users, the use of the description system is as simple as using `?desc location` to retrieve a specific location, or the list of all locations in a neighborhood.  The location descriptions may describe other locations whose descriptions you can retrieve!\n\n"
                    "People with the Wizards role are also able to add, change, and remove descriptions from the system.\n\n"
                    "To add a description, use the `?desc add` command.  It takes the room type in less-than/greater-than brackets, location name, followed by three capital letters in parentheses (optionally with a star), a hyphen, the neighborhood, then the full description, including linebreaks.	Adding the star will cause the description to not be listed in the neighborhood lists or the overall list.  Some examples:\n"
                    "`?desc add <NYC> Katz's Deli (KAT) - Bronx = It's a deli.`\n"
                    "`?desc add <HFC> Polymerase's Petri Dish (*PPD) - Staten Island = This is an unlisted room where Polymerase Does Things (TM).`\n\n"
                    "To remove a description, use the `?desc delete` command.  It will accept either the full name of the room or its short-code as arguments.\n\n"
                    "To change a description without having to delete it, use `?desc edit`.	 This command only accepts the short-code, an equals sign, and then the new description.  No parenthesis are needed for the short-code, but it must use capital letters."
                )
            elif message.content == "?help finger":
                await message.channel.send(
                    "To retrieve a particular character's finger attributes, use `?finger character`.  All finger commands may be used in private message.\n\n"
                    "To set a finger attribute on a character, use `?finger set character attribute = value`.  Some examples:\n"
                    "`?finger set Jane Alignment = Inflexible`\n"
                    "`?finger set Jane Occupation = Mendel Clinic Head of Security`\n\n"
                    "To change a finger attribute on a character, you may simply reset it as if it didn't already exist.  If, however, you want to remove an attribute completely so it no longer shows up, use `?finger unset character attribute`.  For example:\n"
                    "`?finger unset Jane Alignment`\n\n"
                    "All players may set finger attributes on characters they play.	 Members may additionally set finger attributes on NPCs, and Wizards may set finger attributes without restriction."
                )
            else:
                await message.channel.send("I'm not sure what help you're looking for.")

        async def process_log(message):
            c = self.db.cursor()

            if message.content == "?log start":
                logging.info(
                    "Asked to start a log in %s by %s",
                    message.channel.id,
                    str(message.author),
                )
                if message.channel.id not in self.monitored_channels.keys():
                    self.monitored_channels[message.channel.id] = message.id
                    c.execute(
                        "INSERT INTO logs (channel, m_id) VALUES (?, ?)",
                        [message.channel.id, message.id],
                    )
                    self.db.commit()
                    await send_success(message)
                else:
                    # Don't log a channel that's already being logged.
                    await send_failure(message)
            elif message.content == "?log end" or message.content == "?log oocend":
                if message.channel.id in self.monitored_channels.keys():
                    logging.info(
                        "Asked to stop a log in %s by %s",
                        message.channel.id,
                        str(message.author),
                    )
                    # First, create a temp file and fill it with content
                    log = tempfile.TemporaryFile(suffix=".txt")
                    log_id = discord.Object(self.monitored_channels[message.channel.id])
                    logging.info("LOG ID is %s", log_id)
                    async for pose in message.channel.history(
                            after=log_id, before=message, oldest_first=True, limit=None
                    ):
                        if message.content == "?log end":
                            if (
                                not pose.content.startswith("((")
                                and not pose.content.startswith("ooc")
                                and not pose.content.startswith("?desc")
                            ):
                                # We have to specify the encoding here
                                # because the file needs to be opened r+b
                                # for the Discord read later.
                                log.write(wikify(pose.content).encode("utf-8"))
                                log.write("\r\n\r\n".encode("utf-8"))
                        elif message.content == "?log oocend":
                            log.write(
                                "<{}> {}".format(str(pose.author), pose.content).encode(
                                    "utf-8"
                                )
                            )
                            log.write("\r\n\r\n".encode("utf-8"))

                    # Now that we have a file, seek to the beginning and then upload
                    log.seek(0)
                    logging.debug("About to send the file.")
                    await message.channel.send(
                        file=discord.File(
                            fp=log,
                            filename="_".join(
                                [str(datetime.date.today()), message.channel.name]
                            )
                            + ".txt",
                        )
                    )
                    logging.debug("Sent the file.")
                    c.execute(
                        "DELETE from logs where channel = ?", [message.channel.id]
                    )
                    self.db.commit()
                    log.close()
                    del self.monitored_channels[message.channel.id]
                    await send_success(message)
                else:
                    logging.debug("Asked to stop a log in a channel I wasn't logging!")
                    await send_failure(message)
            else:
                logging.debug("Fell through logging system with '%s'", message.content)
                await send_failure(message)

        async def process_role(message):
            if message.content.startswith("?role add"):
                role_name = message.content[10:]
                role = discord.utils.find(
                    lambda x: str(x) == role_name, message.guild.roles
                )

                logging.debug(
                    "Asked to make member %s role %s", str(message.author), role_name
                )

                if check_role(message.author.id, "Member"):
                    if role:
                        logging.debug("Found role; setting...")
                        await message.author.add_roles(role)
                        await send_success(message)
                    else:
                        logging.debug("I don't know that role.")
                        await send_failure(message)
                else:
                    logging.debug("The player isn't authorized to ask for that.")
                    await send_forbidden(message)
            elif message.content.startswith("?role remove"):
                role_name = message.content[13:]
                role = discord.utils.find(
                    lambda x: str(x) == role_name, message.guild.roles
                )

                logging.debug(
                    "Asked to remove role %s from member %s",
                    role_name,
                    str(message.author),
                )

                if str(role) == "Member":
                    logging.debug("Refusing to remove a role equal to Member.")
                    await send_failure(message)
                else:
                    if discord.utils.find(lambda x: x == role, message.author.roles):
                        logging.debug("Role found, removing...")
                        await message.author.remove_roles(role)
                        await send_success(message)
                    else:
                        await send_failure(message)

        async def send_forbidden(message):
            await message.add_reaction("🚫")

        async def send_failure(message):
            await message.add_reaction("✖")

        async def send_success(message):
            await message.add_reaction("✔")

        async def send_long_message(messages, triggering_message):
            msg = ""
            length = 0
            while messages:
                nxt = messages.pop()

                if length + len(nxt) >= 1800:
                    await triggering_message.channel.send(msg)
                    msg = nxt
                    length = len(nxt)
                else:
                    length = length + len(nxt)
                    msg = msg + "\n" + nxt

            await triggering_message.channel.send(msg)

        def wikify(pose):
            slashes = pose.count("/")
            stars = pose.count("*")
            underscores = pose.count("_")
            if slashes and slashes % 2 == 0:
                pose = pose.replace("/", "''")
            if stars and stars % 2 == 0:
                pose = pose.replace("*", "''")
            if underscores and underscores % 2 == 0:
                pose = pose.replace("_", "''")
            return pose
